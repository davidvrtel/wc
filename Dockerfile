FROM node:lts-slim as build-web

WORKDIR /app
COPY ./client/package*.json ./
RUN npm install
COPY ./client .

RUN DATA_URL=/data ALERTTARGETS_URL=/alerttargets npm run build

FROM denoland/deno:1.18.1 as production-stage

WORKDIR /app

ENV TZ=Europe/Prague
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

ENV SETTINGS_CONFIG_FILE_PATH=/app/config/settings.json
ENV ALERT_CONFIG_FILE_PATH=/app/config/alerting.json
ENV LOG_LEVEL=INFO
ENV CORS=false
ENV HEADLESS=false

USER deno

COPY ./backend/deps.ts ./
RUN deno cache deps.ts

ADD ./backend ./
RUN deno cache --unstable main.ts

COPY --from=build-web /app/public /app/client

EXPOSE 8080
CMD ["run", "--allow-net", "--allow-env", "--allow-read", "--unstable", "main.ts"]

