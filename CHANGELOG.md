# CHANGELOG

This is changelog with all released versions and changes.

## v0.3.0 (20-Feb-2022)

### New features

- Improved client displaying alert status and alert targets
- Client UI section added to docs.

## v0.2.0 (20-Feb-2022)

### New features

- Improved logging and error handling
- Added option to set CORS
- Added headless mode
- Documentation updates

## v0.1.4 (20-Feb-2022)

### Bugfixing

- Fixing CI for pages

## v0.1.3 (20-Feb-2022)

### New features

- Documentation finished and added to releases
- Pages for docs

## v0.1.2 (19-Feb-2022)

### New features

- Improving my quality of life


## v0.1.1 (7-Feb-2022)

### New features

- CI setup with tagging and releases
- Improving my quality of life

### Bug fixes

- none

## v0.1.0 (6-Feb-2022)

### New features

- Initial release
