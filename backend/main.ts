import { Application, Router, oakCors } from "./deps.ts";
import { logger } from "./handlers/logger.ts";

import { monitoredWebsites } from "./handlers/config.ts";
import {
    data,
    handleResults,
    alertStatus,
    alertTargets,
} from "./handlers/worker.ts";

// initiation of worker threads
monitoredWebsites.forEach((website, index) => {
    const worker = new Worker(new URL("./worker.ts", import.meta.url).href, {
        type: "module",
        deno: {
            namespace: true,
        },
    });
    worker.onmessage = handleResults;
    worker.postMessage({ website, index });

    logger.info(`Creating worker for ${website.url}`);
});

// initiation of Oak webserver
const app = new Application();
const router = new Router();

// routes for data fetching and serving static web
router.get("/data", (ctx) => {
    ctx.response.body = { monitoredWebsites, data, alertStatus };
});

router.get("/alerttargets", (ctx) => {
    ctx.response.body = { alertTargets };
});

// logger
app.use(async (ctx, next) => {
    await next();
    logger.info(
        `WS: ${ctx.request.method} ${ctx.response.status} ${
            ctx.request.url
        } ${ctx.request.headers.get("User-Agent")}`
    );
});

// composition and start of webserver
if (Deno.env.get("HEADLESS") === "false") {
    app.use(async (context, next) => {
        try {
            await context.send({
                root: `${Deno.cwd()}/client`,
                index: "index.html",
            });
        } catch {
            next();
        }
    });
}

if (Deno.env.get("CORS") === "true") {
    app.use(oakCors({ origin: "*" }));
}

app.use(router.routes());

console.log(
    `
======================= Welcome to ========================
    __      __   _       ___ _           _
    \\ \\    / /__| |__   / __| |_  ___ __| |_____ _ _
     \\ \\/\\/ / -_) '_ \\ | (__| ' \\/ -_) _| / / -_) '_|
      \\_/\\_/\\___|_.__/  \\___|_||_\\___\\__|_\\_\\___|_|

            https://gitlab.com/davidvrtel/wc

===========================================================
    `
);

logger.info(`Starting server on localhost:8080`);

await app.listen({ port: 8080 });
