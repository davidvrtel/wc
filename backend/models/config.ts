export interface Rules {
    failureRate: number; // must be <= history
    successRate: number; // must be <= history
}

export interface Settings {
    history: number;
    interval: number;
    timeout: number;
}

export interface WebInstance {
    url: string;
    settings: Settings;
    rules: Rules;
}

export interface IndexedWebInstance {
    website: WebInstance;
    index: number;
}

export interface FetchResult {
    code: number;
    message: string;
    timestamp: string;
}

export interface Config {
    settings: Settings;
    definition: WebInstance[];
}

export type MonitoredWebsites = WebInstance[];
