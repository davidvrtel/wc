import { logger } from "../handlers/logger.ts";

type WebhookType = "discord" | "teams" | "slack" | "custom";

export interface AlertConfig {
    url: string;
    type: WebhookType;
    body?: string;
}

export class WebhookTarget {
    url: string;
    type: WebhookType;
    body: string;

    constructor(url: string, type: WebhookType, body?: string) {
        this.url = url;
        this.type = type;
        if (body) {
            this.body = body;
            logger.info(
                `Alerting target of type '${type}' with custom body created.`
            );
            logger.debug(`Alerting target URL: ${url}`);
        } else {
            switch (type) {
                case "discord":
                    this.body = JSON.stringify({
                        content: "@here",
                        embeds: [
                            {
                                title: "Alerting website: WEBSITE_URL",
                                description:
                                    "Go check your instance of Web Checker!",
                                url: "WEBSITE_URL",
                                color: 16711680,
                            },
                        ],
                    });
                    break;
                case "teams":
                    this.body = JSON.stringify({
                        type: "message",
                        attachments: [
                            {
                                contentType:
                                    "application/vnd.microsoft.card.adaptive",
                                contentUrl: null,
                                content: {
                                    $schema:
                                        "http://adaptivecards.io/schemas/adaptive-card.json",
                                    type: "AdaptiveCard",
                                    version: "1.2",
                                    body: [
                                        {
                                            type: "TextBlock",
                                            text: "Alerting website: WEBSITE_URL",
                                            style: "heading",
                                            size: "Large",
                                            weight: "Bolder",
                                        },
                                        {
                                            type: "TextBlock",
                                            text: "Go check your instance of Web Checker!",
                                        },
                                        {
                                            type: "ActionSet",
                                            actions: [
                                                {
                                                    type: "Action.OpenUrl",
                                                    title: "Open WEBSITE_URL",
                                                    url: "WEBSITE_URL",
                                                },
                                            ],
                                        },
                                    ],
                                },
                            },
                        ],
                    });
                    break;
                case "slack":
                    this.body = JSON.stringify({
                        blocks: [
                            {
                                type: "header",
                                text: {
                                    type: "plain_text",
                                    text: "Alerting website: WEBSITE_URL",
                                    emoji: true,
                                },
                            },
                            {
                                type: "section",
                                text: {
                                    type: "mrkdwn",
                                    text: "Go check your instance of Web Checker!",
                                },
                            },
                            {
                                type: "actions",
                                elements: [
                                    {
                                        type: "button",
                                        text: {
                                            type: "plain_text",
                                            text: "Open WEBSITE_URL",
                                            emoji: true,
                                        },
                                        value: "open-website-url",
                                        url: "WEBSITE_URL",
                                        action_id: "button-action",
                                    },
                                ],
                            },
                        ],
                    });
                    break;
                case "custom":
                    if (body) {
                        this.body = body;
                    } else {
                        logger.warning(`Alert target setup interrupted.`);
                        throw new Deno.errors.InvalidData(
                            `Alerting target of type 'custom' does not have body specified for alerting url: ${url}`
                        );
                    }
                    break;
                default:
                    logger.warning(`Alert target setup interrupted.`);
                    throw new Deno.errors.InvalidData(
                        `Alerting target of type '${type}' doesn't exist. Not set.`
                    );
            }

            logger.info(
                `Alerting target of type '${type}' with default body created.`
            );
            logger.debug(`Alerting target URL: ${url}`);
        }
    }

    async send(monitoredUrl: string) {
        try {
            const controller = new AbortController();
            const id = setTimeout(() => controller.abort(), 20000);

            await fetch(this.url, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                },
                body: this.body.replaceAll("WEBSITE_URL", monitoredUrl),
                signal: controller.signal,
            });

            clearTimeout(id);

            logger.info(
                `Alert sent to ${this.type} target for ${monitoredUrl}`
            );
        } catch (e) {
            if (e instanceof TypeError) {
                logger.error(`Could not send alert to ${this.type} target.`);
                logger.error(`${e}`);
            }

            if (e instanceof DOMException) {
                logger.error(
                    `Alert sending aborted for ${this.type} target, POST request taking longer than 20 seconds.`
                );
            }
        }
    }
}
