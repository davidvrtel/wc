import "https://deno.land/x/dotenv/load.ts";
import { logger } from "./logger.ts";
import type {
    Config,
    Settings,
    MonitoredWebsites,
    WebInstance,
} from "../models/config.ts";

const HISTORY = 30;
const INTERVAL = 30;
const TIMEOUT = 30;
const MIN_HISTORY = HISTORY;
const MIN_INTERVAL = 5;
const MIN_TIMEOUT = 5;

// start of builder
const buildMonitoredWebsites = async (): Promise<MonitoredWebsites> => {
    // read settings.json from env var and check contents
    // terminate if no value is set or if no content is present
    const configPath = Deno.env.get("SETTINGS_CONFIG_FILE_PATH");

    if (!configPath) {
        throw new Deno.errors.NotFound(
            "Config file path not set! Make sure CONFIG_FILE_PATH is set to config.json"
        );
    }

    // get input config and parse it
    let inputConfig: Config;

    try {
        const inputConfigRaw = await Deno.readTextFile(configPath);
        inputConfig = JSON.parse(inputConfigRaw);
    } catch {
        logger.critical(
            `Config on path ${configPath} doesn't exist. Terminating.`
        );
        Deno.exit(1);
    }

    logger.debug("Input config file: ");
    logger.debug(JSON.stringify(inputConfig));

    // default settings builder returns default values specified as constants on top of this file
    type Identifier = "history" | "interval" | "timeout";

    const buildDefaultSettings = (config: Config): Settings => {
        logger.debug(`----- Generating default settings -----`);

        const validateAndSet = (
            identifier: Identifier,
            defaultValue: number,
            minValue: number
        ) => {
            if (
                config.settings?.[identifier]?.valueOf() !== undefined &&
                config.settings[identifier] >= minValue
            ) {
                return config.settings[identifier];
            } else {
                logger.debug(
                    `Value of supplied default ${identifier} is (${config.settings[identifier]}) either too small (less than ${minValue}) or undefined. Using ${defaultValue}`
                );
                return defaultValue;
            }
        };

        const defaultSettings: Settings = {
            history: validateAndSet("history", HISTORY, MIN_HISTORY),
            interval: validateAndSet("interval", INTERVAL, MIN_INTERVAL),
            timeout: validateAndSet("timeout", TIMEOUT, MIN_TIMEOUT),
        };

        return defaultSettings;
    };

    const defaultSettings = buildDefaultSettings(inputConfig);

    // terminate if no websites are specified
    if (inputConfig.definition.length === 0) {
        throw new Deno.errors.NotFound(
            `No websites to monitor specified in: ${configPath}`
        );
    }

    const validateAndSetSettings = (
        web: WebInstance,
        identifier: Identifier,
        defaults: Settings,
        minValue: number
    ) => {
        if (
            web.settings?.[identifier]?.valueOf() !== undefined &&
            web.settings[identifier] > minValue
        ) {
            return web.settings[identifier];
        } else {
            logger.debug(
                `Value of ${identifier} for '${web.url}' is either too small (less than ${minValue}) or undefined. Using ${defaults[identifier]}`
            );
            return defaults[identifier];
        }
    };

    // following section validates and sets failure and success rate
    type Rate = "failureRate" | "successRate";

    const validateAndSetRules = (
        web: WebInstance,
        identifier: Rate,
        history: number
    ) => {
        if (
            web.rules?.[identifier]?.valueOf() !== undefined &&
            web.rules[identifier] <= history &&
            web.rules[identifier] > 0
        ) {
            return web.rules[identifier];
        } else {
            logger.debug(
                `Value of ${identifier} must be smaller or equal to history (${history}), but not 0 or less. '${web.url}' value: ${web.rules?.[identifier]}`
            );
            return -1;
        }
    };

    const monitoredWebsites: MonitoredWebsites = inputConfig.definition.map(
        (web) => {
            logger.info(`----- Setting up ${web.url} -----`);

            const history = validateAndSetSettings(
                web,
                "history",
                defaultSettings,
                MIN_HISTORY
            );

            const interval = validateAndSetSettings(
                web,
                "interval",
                defaultSettings,
                MIN_INTERVAL
            );

            const timeout = validateAndSetSettings(
                web,
                "timeout",
                defaultSettings,
                MIN_TIMEOUT
            );

            const failureRate = validateAndSetRules(
                web,
                "failureRate",
                history
            );

            const successRate = validateAndSetRules(
                web,
                "successRate",
                history
            );

            const monitoredWeb: WebInstance = {
                url: web.url,
                settings: {
                    history,
                    interval,
                    timeout,
                },
                rules: {
                    failureRate,
                    successRate,
                },
            };

            if (
                monitoredWeb.rules.failureRate === -1 ||
                monitoredWeb.rules.successRate === -1
            ) {
                logger.warning(`Alerting for '${web.url}' disabled.`);
                monitoredWeb.rules = {
                    failureRate: -1,
                    successRate: -1,
                };
            }

            return monitoredWeb;
        }
    );

    return monitoredWebsites;
};

export const monitoredWebsites = await buildMonitoredWebsites();
logger.debug("Output of parsed monitored websites: ");
logger.debug(JSON.stringify(monitoredWebsites));
