import { monitoredWebsites } from "./config.ts";
import type { AlertConfig } from "../models/alerting.ts";
import { WebhookTarget } from "../models/alerting.ts";
import { logger } from "./logger.ts";
import type { IndexedWebInstance, FetchResult } from "../models/config.ts";

// handler manipulating the data posted by worker thread
export const handleResults = (
    e: MessageEvent<{ instance: IndexedWebInstance; result: FetchResult }>
) => {
    const { instance, result } = e.data;
    const index = instance.index;
    const url = instance.website.url;
    const failureRate = instance.website.rules.failureRate;
    const successRate = instance.website.rules.successRate;

    if (result) {
        data[index].shift();
        data[index].push(result);
    }

    if (failureRate !== -1 && successRate !== -1) {
        const checkFailure = (code: number) => {
            return code === 404 || code === 408 || code >= 500;
        };

        switch (alertStatus[index]) {
            case false: {
                const isFailure = data[index]
                    .slice(-failureRate)
                    .every((element) => {
                        return checkFailure(element.code);
                    });
                if (isFailure) {
                    alertStatus[index] = true;
                    logger.info(`Alert triggered for ${url}`);
                    alertTargets.forEach(
                        async (target) => await target.send(url)
                    );
                }
                break;
            }
            case true: {
                const isSuccess = data[index]
                    .slice(-successRate)
                    .every((element) => {
                        return !checkFailure(element.code);
                    });
                if (isSuccess) {
                    alertStatus[index] = false;
                    logger.info(`Alert reset for ${url}`);
                }
                break;
            }
        }

        logger.debug(`Alert status for ${url}: ${alertStatus[index]}`);
    }
};

const initiateWebhookTargets = async () => {
    const configPath = Deno.env.get("ALERT_CONFIG_FILE_PATH");

    if (!configPath) {
        logger.warning("Alert config file path not set! Alerting disabled.");
        logger.warning(
            "Make sure ALERT_CONFIG_FILE_PATH is set to alerting.json in case you want to enable alerting."
        );
        return [];
    }

    let alertConfig: AlertConfig[];

    try {
        const alertConfigRaw = await Deno.readTextFile(configPath);
        alertConfig = JSON.parse(alertConfigRaw);
    } catch {
        logger.warning(
            `Can't find alert targets in ${configPath}. Alerting disabled.`
        );
        return [];
    }

    logger.debug("Input alerting.json config file:");
    logger.debug(JSON.stringify(alertConfig));

    return alertConfig
        .map((item) => {
            try {
                return new WebhookTarget(item.url, item.type, item.body);
            } catch (e) {
                logger.critical(e);
                return undefined;
            }
        })
        .filter((item): item is WebhookTarget => !!item);
};

// latest results of fetching data
export const data: FetchResult[][] = Array.from(
    Array(monitoredWebsites.length),
    (_, i) =>
        new Array(monitoredWebsites[i].settings.history).fill({
            code: 102,
            message: "Processing",
            timestamp: "",
        })
);

// true for triggered alert, false for recovered
export const alertStatus = new Array(monitoredWebsites.length).fill(false);

// creating alerting targets
export const alertTargets: WebhookTarget[] = await initiateWebhookTargets();
