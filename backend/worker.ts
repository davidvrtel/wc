import type { IndexedWebInstance, FetchResult } from "./models/config.ts";

enum LogLevel {
    DEBUG = 1,
    INFO = 2,
    WARNING = 4,
    ERROR = 8,
    CRITICAL = 16,
}

type LogLevelName = keyof typeof LogLevel;

class Logger {
    level: LogLevelName;
    levelWeight: LogLevel;

    constructor(level: LogLevelName) {
        this.level = level;

        switch (level) {
            case "DEBUG":
                this.levelWeight = 1;
                break;
            case "INFO":
                this.levelWeight = 2;
                break;
            case "WARNING":
                this.levelWeight = 4;
                break;
            case "ERROR":
                this.levelWeight = 8;
                break;
            case "CRITICAL":
                this.levelWeight = 16;
                break;
            default:
                this.levelWeight = 2;
                break;
        }
    }

    getTimestamp() {
        return new Date().toISOString();
    }

    log(level: LogLevelName, msg: string) {
        let spacesAfter = "";
        let prefix = "";
        const timestamp = this.getTimestamp();

        switch (level) {
            case "DEBUG":
                spacesAfter = "     ";
                prefix = "\x1b[34mDEBUG\x1b[0m";
                break;
            case "INFO":
                spacesAfter = "      ";
                prefix = "INFO";
                break;
            case "WARNING":
                spacesAfter = "   ";
                prefix = "\x1b[33mWARNING\x1b[0m";
                break;
            case "ERROR":
                spacesAfter = "     ";
                prefix = "\x1b[31mERROR\x1b[0m";
                break;
            case "CRITICAL":
                spacesAfter = "  ";
                prefix = "\x1b[31;1mCRITICAL\x1b[0m";
                break;
            default:
                break;
        }

        console.log(`${timestamp} ${prefix}${spacesAfter}${msg}`);
    }

    debug(message: string) {
        if (this.levelWeight <= LogLevel.DEBUG) {
            this.log("DEBUG", message);
        }
    }

    info(message: string) {
        if (this.levelWeight <= LogLevel.INFO) {
            this.log("INFO", message);
        }
    }

    warning(message: string) {
        if (this.levelWeight <= LogLevel.WARNING) {
            this.log("WARNING", message);
        }
    }

    error(message: string) {
        if (this.levelWeight <= LogLevel.ERROR) {
            this.log("ERROR", message);
        }
    }

    critical(message: string) {
        if (this.levelWeight <= LogLevel.CRITICAL) {
            this.log("CRITICAL", message);
        }
    }
}

const logLevel = Deno.env.get("LOG_LEVEL");
const logger = new Logger(logLevel ? (logLevel as LogLevelName) : "INFO");

// worker thread starts processing on receiving config data
onmessage = (e: MessageEvent<IndexedWebInstance>) => {
    const instance = e.data;
    const url = instance.website.url;
    const interval = instance.website.settings.interval * 1000;
    const timeout = instance.website.settings.timeout * 1000;

    // timeout, interval and websites are specified in config.json
    setInterval(async () => {
        let result: FetchResult;

        try {
            const controller = new AbortController();
            const id = setTimeout(() => controller.abort(), timeout);

            const response = await self.fetch(url, {
                signal: controller.signal,
            });

            clearTimeout(id);

            // const timestamp = formatDate(new Date());
            const timestamp = new Date().toISOString();

            result = {
                code: response.status,
                message: response.statusText,
                timestamp,
            };

            logger.debug(
                `WW: Fetched ${url} with result: ${result.code} ${result.message}`
            );
        } catch (e) {
            const timestamp = new Date().toISOString();

            result = {
                code: 408,
                message: "Request Timeout",
                timestamp,
            };

            if (e instanceof TypeError) {
                logger.error(`WW: Error occured for ${url}`);
                logger.error(`WW: ${e}`);
                logger.warning(`WW: Returning 408 Request Timeout for ${url}`);
            }

            if (e instanceof DOMException) {
                logger.warning(
                    `WW: Fetch aborted for ${url}, request taking longer than timeout definition allows.`
                );
                logger.warning(`WW: Returning 408 Request Timeout for ${url}`);
            }
        }

        postMessage({ instance, result });
    }, interval);
};
