# Web Checker (WC)
Tool is gathering shit your website drops and flushes it down the sewage for you to pick up on the other side, should any rule you want match.

Just kidding, it simply notifies you when websites of interest might be down.

Repository: [https://gitlab.com/davidvrtel/wc](https://gitlab.com/davidvrtel/wc)</br>
Documentation: [https://gitlab.com/davidvrtel/wc](https://gitlab.com/davidvrtel/wc)</br>
Docker Hub: [https://hub.docker.com/r/davidvrtel/web-checker](https://hub.docker.com/r/davidvrtel/web-checker)</br>

NOTE: WC is simple tool for simple usage, it will break if you try to break it. Runs on Deno unstable version for webworker to work properly. History is not persisted (kept in memory).


## Quick start
Best way to run WC is in docker. First, you have to specify some websites to monitor. Optionally, set up alerting targets.

Create `settings.json` and populate with monitored websites and additional settings.
```JSON
{
    "settings": {
        "history": 50,
        "interval": 20,
        "timeout": 15
    },
    "definition": [
        {
            "url": "http://example.com",
            "settings": {
                "history": 200
            }
        },
        {
            "url": "http://httpstat.us/301",
            "settings": {
                "history": 20
            },
            "rules": {
                "failureRate": 5
            }
        },
        {
            "url": "http://httpstat.us/503",
            "rules": {
                "failureRate": 5,
                "successRate": 5
            }
        },
        {
            "url": "http://httpstat.us/418"
        }
    ]
}
```

Run docker image and mount `settings.json`
```bash
docker run --name webmon -d --rm -p 8080:8080 -v $(pwd)/settings.json:/app/config/settings.json davidvrtel/web-checker:latest
```

Visit `http://localhost:8080` in your browser.


## TODO
- error handling (client)
- client improvements
