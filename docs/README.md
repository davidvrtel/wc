# Web Checker (WC)
Tool is gathering shit your website drops and flushes it down the sewage for you to pick up on the other side, should any rule you want match.

Just kidding, it simply notifies you when websites of interest might be down.

Repository: [https://gitlab.com/davidvrtel/wc](https://gitlab.com/davidvrtel/wc)</br>
Documentation: [https://gitlab.com/davidvrtel/wc](https://gitlab.com/davidvrtel/wc)</br>
Docker Hub: [https://hub.docker.com/r/davidvrtel/web-checker](https://hub.docker.com/r/davidvrtel/web-checker)</br>

NOTE: WC is simple tool for simple usage, it will break if you try to break it. Runs on Deno unstable version for webworker to work properly. History is not persisted (kept in memory).

## Quick start
Best way to run WC is in docker. First, you have to specify some websites to monitor. Optionally, set up alerting targets (explained later).

Create `settings.json` and populate with monitored websites and additional settings (structure explained [settings.json](#settingsjson) section).
```JSON
{
    "settings": {
        "history": 50,
        "interval": 20,
        "timeout": 15
    },
    "definition": [
        {
            "url": "http://example.com",
            "settings": {
                "history": 200
            }
        },
        {
            "url": "http://httpstat.us/301",
            "settings": {
                "history": 20
            },
            "rules": {
                "failureRate": 5
            }
        },
        {
            "url": "http://httpstat.us/503",
            "rules": {
                "failureRate": 5,
                "successRate": 5
            }
        },
        {
            "url": "http://httpstat.us/418"
        }
    ]
}
```

Run docker container and mount `settings.json`
```bash
docker run --name webmon -d --rm -p 8080:8080 -v $(pwd)/settings.json:/app/config/settings.json davidvrtel/web-checker:latest
```

Visit `http://localhost:8080` in your browser.


## How it works
WC has backend and client bundled in one docker image. Alternatively, you can build entire solution and run it locally, shoud all prerequisities are met.

Backend periodically fetches websites specified in config file. Interval, timeout and history for these fetches can be configured globally or for each website separately. Failure and success rate for alerting can also be specified for each website. If no values (global nor individual) are set, default are used (more in [Config](#config) section).

Fetching happens in web workers. Each website gets it's own web worker. HTTP status code, status message and timestamp are returned after each fetch. If request takes longer than timeout value, request timeouts and returns 408.

Client side of WC then fetches those results and displays latest states in [dashboard](#screenshots). Entire history can be accessed for each monitored websites from client.

Alerts can be sent via webhooks by specifying alerting targets. Predefined platforms are discord, teams and slack. User can also change webhook body for predefined platforms or specify other webhook targets with custom body. See [alerting.json](#alertingjson) for details.

Alerts are sent based on failure and success rates.

WC does not check if URL is correct, nor does it check if alerting target is really there. It simply attempts to fetch the website and sends webhook request, should any of the rules are met.


## How to use
WC can be run either as [docker container](#docker-setup) (recommended) or [locally](#local-setup-without-docker). Config must be set properly with at least one website to monitor, otherwise it won't start. Aleting targets are optional.

Each of the following sections explains how to use WC in detail.


### Config
There are two config types, required `settings.json` and optional `alerting.json`.

#### settings.json
Structure is defined with following code:
```typescript
interface Config {
    settings?: {
        history?: number;
        interval?: number;
        timeout?: number;
    };
    definition: {
        url: string;
        settings?: {
            history?: number;
            interval?: number
            timeout?: number;
        };
        rules?: {
            failureRate: number;
            successRate: number;
        };
    }[];
}
```

Described with words, Config object contains `settings` and `definition` sections. Definition is array of websites to monitor. Only `url` is required. All `settings` fields are optional.


##### settings field (website definition)
Settings allow us to set history, interval and timeout both globally and individually. Defaults are set as follows. Values set by users must be bigger than defined minimum 30.
``` typescript
const HISTORY = 30;
const INTERVAL = 30;
const TIMEOUT = 30;
const MIN_HISTORY = HISTORY;
const MIN_INTERVAL = 5;
const MIN_TIMEOUT = 5;
```

For example, in [Quick start](#quick-start) example, global settings are changed to `"history": 50, "interval": 20, "timeout": 15`, this will be applied to every monitored website.

Two websites have individual settings defined, `"history": 200` works fine because it's bigger than allowed minimum. However, `"history": 20` won't be applied and allowed minimum value of 30 will be used.


##### rules field (alert triggering)
Last part is `rules` field which is optional. Rules are used to indicate if alert should be sent or not. Both `failureRate` and `successRate` must be set and smaller than or equal to `history` for that website in order for alerts to work.

In [Quick start](#quick-start) example, we have two websites with `"rules"` field. First one has only `"failureRate"` set to 5, but due to the absence of `"successRate"`, alerts are disabled. In case of website where both failure and success rate are set to 5, alerting is enabled and working.

Failure is when HTTP code is 404, 408 or any 5xx. Failure rate defines how many failures there must be to sent an alert. If any other code is fetched, success rate defines how many of those in row are needed for alerting status to be reset. Another alert is sent only if alerting status is reset and failure rate fullfiled again.

#### alerting.json
Structure can be described as array of following objects:
```typescript
interface AlertConfig {
    url: string;
    type: "discord" | "teams" | "slack" | "custom";
    body?: string;
}
```

Alert is sent as webhook (POST request) to specified URL with either predefined body or body specified in this config.

Only information about which website has triggered the alert is URL. This is specified in JSON body with `WEBSITE_URL` placeholder string which is replaced before sending. Add this string anywhere to your custom body for URL to be sent in alert.

All three predefined types (discord, teams and slack) have predefined simple bodies shown in following section. Custom type must have body specified otherwise no alerts will be sent!


##### Alert body
For more information about webhooks for these platforms, see following links:
- Discord ([Docs](https://discord.com/developers/docs/resources/webhook), [Discohook](https://discohook.org/))
- Teams ([Docs](https://docs.microsoft.com/en-us/microsoftteams/platform/webhooks-and-connectors/how-to/add-incoming-webhook), [Adaptive Cards Designer](https://adaptivecards.io/designer/))
- slack ([Docs](https://api.slack.com/messaging/webhooks), [Block Kit Builder](https://app.slack.com/block-kit-builder))

Default bodies:
<details>
  <summary>Default discord body</summary>

  ```JSON
    {
    "content": "@here",
    "embeds": [
        {
        "title": "Alerting website: WEBSITE_URL",
        "description": "Go check your instance of Web Checker!",
        "url": "WEBSITE_URL",
        "color": 16711680
        }
    ]
    }
  ```
</details>

<details>
  <summary>Default teams body</summary>

  ```JSON
    {
    "type": "message",
    "attachments": [
        {
        "contentType": "application/vnd.microsoft.card.adaptive",
        "contentUrl": null,
        "content": {
            "$schema": "http://adaptivecards.io/schemas/adaptive-card.json",
            "type": "AdaptiveCard",
            "version": "1.2",
            "body": [
            {
                "type": "TextBlock",
                "text": "Alerting website: WEBSITE_URL",
                "style": "heading",
                "size": "Large",
                "weight": "Bolder"
            },
            {
                "type": "TextBlock",
                "text": "Go check your instance of Web Checker!"
            },
            {
                "type": "ActionSet",
                "actions": [
                {
                    "type": "Action.OpenUrl",
                    "title": "Open WEBSITE_URL",
                    "url": "WEBSITE_URL"
                }
                ]
            }
            ]
        }
        }
    ]
    }
  ```
</details>

<details>
  <summary>Default slack body</summary>

  ```JSON
    {
    "blocks": [
        {
        "type": "header",
        "text": {
            "type": "plain_text",
            "text": "Alerting website: WEBSITE_URL",
            "emoji": true
        }
        },
        {
        "type": "section",
        "text": {
            "type": "mrkdwn",
            "text": "Go check your instance of Web Checker!"
        }
        },
        {
        "type": "actions",
        "elements": [
            {
            "type": "button",
            "text": {
                "type": "plain_text",
                "text": "Open WEBSITE_URL",
                "emoji": true
            },
            "value": "open-website-url",
            "url": "WEBSITE_URL",
            "action_id": "button-action"
            }
        ]
        }
    ]
    }
  ```
</details>

### Logging
Logger outputs every log message to stdout. Log levels are:
- DEBUG
- INFO
- WARNING
- ERROR
- CRITICAL

Log levels can be changed by setting `LOG_LEVEL` environment variable.

Default is INFO, the most verbose is DEBUG. Log level defines the lowest severity output and suppresses every other message. For example setting `LOG_LEVEL=WARNING` tells logger to not log any DEBUG and INFO messages.

Structure of log message is `TIMESTAMP LEVEL MESSAGE` and there are three types of messages depending on what the message starts with:
- `WS:` is web server log, displays only requests to API and client, message consists of HTTP method, response code, URI and User-Agent header
- `WW:` is web worker log, those are messages from inside of web workers fetching monitored websites

```log
2022-02-20T14:16:04.615Z INFO      WS: GET 200 http://localhost:8080/data Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.87 Safari/537.36
2022-02-20T14:16:09.130Z ERROR     WW: Error occured for tcp://dada.dada
2022-02-20T14:16:09.130Z ERROR     WW: TypeError: scheme 'tcp' not supported
2022-02-20T14:16:09.130Z WARNING   WW: Returning 408 Request Timeout for tcp://dada.dada
2022-02-20T14:16:09.315Z DEBUG     WW: Fetched http://httpstat.us/503 with result: 503 Service Unavailable
2022-02-20T14:16:09.315Z DEBUG     Alert status for http://httpstat.us/503: true

```

### Docker setup
Running WC in Docker is recommended. Having Docker installed on machine where you want to run WC is necessary for this option to work.

Create `settings.json` and `alerting.json` and mount those to docker container to default paths. There are three environment variables that you can change.

```bash
# default values
SETTINGS_CONFIG_FILE_PATH=/app/config/settings.json     #required
ALERT_CONFIG_FILE_PATH=/app/config/alerting.json        #optional
LOG_LEVEL=INFO                                          #optional
CORS=false                                              #optional
HEADLESS=false                                          #optional
```

CORS can be set to `true` if you want to access API from other sources. See [Headless version](#headless) for more info.

Changing the paths must be reflexted in `docker run` command accordingly.

With configuration prepared, start the docker container. You can also run it with compose.

```bash
docker run --name webmon -d --rm \
    -p 8080:8080  \
    -v $(pwd)/settings.json:/app/config/settings.json \
    -v $(pwd)/alerting.json:/app/config/alerting.json \
    -e LOG_LEVEL=DEBUG \
    davidvrtel/web-checker:latest
```

Visit `http://localhost:8080` in your browser.

#### Headless
You can also run WC without client in case you want to fetch data by other tools. Two environment variables take care of that
- `CORS` has to be set to `true` to allow any origins to access the API
- `HEADLESS` has to be set to true if you want to disable client

API endpoints:
- `GET /data` returns JSON of monitored website definitions, history of fetch results and alert status array. Arrays are ordered and items on index for each of the three belong together.
    - `monitoredWebsites` contain settings and rules, if rules have -1 values it means alerting is disabled.
    - `data` contain history of fetch results. Each item is array of size of corresponding history field. Order of each array is ascending (recent fetch results are on last index).
    - `alertStatus` states if alert was triggered (true) or reset (false, also can mean alert was never sent).
```json
{
    "monitoredWebsites": [
        {
            "url": "tcp://dada.com",
            "settings": {
                "history": 200,
                "interval": 20,
                "timeout": 15
            },
            "rules": {
                "failureRate": 10,
                "successRate": 20
            }
        },
        {
            "url": "http://httpstat.us/301",
            "settings": {
                "history": 50,
                "interval": 20,
                "timeout": 15
            },
            "rules": {
                "failureRate": -1,
                "successRate": -1
            }
        }
    ],
    "data": [
        [
            {
                "code": 408,
                "message": "Request Timeout",
                "timestamp": "2022-02-20T14:05:29.072Z"
            }
        ],
        [
            {
                "code": 200,
                "message": "OK",
                "timestamp": "2022-02-20T14:05:29.107Z"
            }
        ]
    ],
    "alertStatus": [
        true,
        false
    ]
}
```
- `GET /alerttargets` returns JSON of alert targets definitions. **IMPORTANT:** be careful to expose this endpoint since it contains your webhook links
```json
{
  "alertTargets": [
    {
      "url": "hattps://discordapp.com/api/webhooks/xxx",
      "type": "discord",
      "body": "..."
    },
    {
      "url": "https://adastrabiz.webhook.office.com/webhookb2/xxx",
      "type": "teams",
      "body": "..."
    },
    {
      "url": "https://hooks.slack.com/services/xxx",
      "type": "slack",
      "body": "..."
    }
  ]
}
```

Run headless WC in docker container with:
```bash
docker run --name webmon -d --rm \
    -p 8080:8080  \
    -v $(pwd)/settings.json:/app/config/settings.json \
    -v $(pwd)/alerting.json:/app/config/alerting.json \
    -e CORS=true \
    -e HEADLESS=true \
    davidvrtel/web-checker:latest
```

### Local setup (without Docker)
Having NPM and Deno installed is must for this to work locally. Some env variables needs to be set for solution to work properly.

Clone the repository `git clone git@gitlab.com:davidvrtel/wc.git` and `cd wc`.

To run complete project locally:

1. Go to `client/` directory and run
    ```bash
    DATA_URL=/data ALERTTARGETS_URL=/alerttargets npm run build
    ```
2. Edit `settings.json` and `alerting.json` files to your likings. Go to `backend/` directory and run
    ```bash
    SETTINGS_CONFIG_FILE_PATH=./config/settings.json \       # required, path to settings.json
    ALERT_CONFIG_FILE_PATH=./config/alerting.json \          # optional, path to alerting.json
    LOG_LEVEL=DEBUG \                                        # optional, default is INFO
    CORS=false \                                             # optional, default is false
    HEADLESS=false \                                         # optional, default is false
    deno run --allow-net --allow-read --allow-env --unstable main.ts
    ```
3. Visit `http://localhost:8080` in your browser.


### Client UI
Client UI is designed to display all the necessary information collected by backend.

Header section displays last update of fetched data which happens every 5s. It also displays how many alert targets are configured.

Dashboard part displays each monitored website as cards with clickable link, favicon (if existing, fetches favicon.png) and status code, message and timestamp of last fetch. Card also shows if alerting is enabled and if so, whether the alert status is OK or triggered.

Button 'View logs' displays modal with entire history of fetches on that website.

Status code and message are in colored tag with 100, 200 and 300 codes being green, 400 being yellow and 500 being red.

Should alert be triggered, card of corresponding website glows red.

![Alert status](/08.png)


### HTTPS/SSL
For simplicity purposes, SSL for secure communication is not supported by WC. Use reverse proxy to provide HTTPS.

## Screenshots

![Dashboard](/01.png)

![Log detail](/02.png)

![Mobile dashboard](/03.png)

![Discord notification](/04.png)

![Teams notification](/05.png)

![Slack notification](/06.png)

![Logs](/07.png)

![Alert status](/08.png)

